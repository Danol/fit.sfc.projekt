QT_DIR := /usr/local/share/Qt-5.9.2/5.9.2/gcc_64/

QT_BINARIES_DIR := $(QT_DIR)bin/
QT_PLUGINS_DIR := $(QT_DIR)plugins

.PHONY: gui
gui: | $(QT_DIR)
	(mkdir -p build && cd build && $(QT_BINARIES_DIR)qmake ../src/SFC_projekt_xcejch00.pro && make)

.PHONY: all
all: gui

.PHONY: run
run: gui
	export QT_PLUGIN_PATH=$(QT_PLUGINS_DIR) && cd bin && ./SFC_projekt_xcejch00

$(QT_DIR):
	@echo "Variable QT_DIR='$(QT_DIR)' links to an unexisting folder. Update the QT_DIR variable in Makefile and try again"
	@exit 1

.PHONY: clean
clean:
	rm -rf build
	rm bin/SFC_projekt_xcejch00