import std.stdio;
import std.algorithm;
import std.file;
import std.string;
import std.array;

void main(string[] args) {
	auto outputFile = File("data.csv", "w");
	immutable string[string] types = [
		"N": "1",
		"A": "2",
		"P": "3",
		"C": "4",
		"V": "5",
		"D": "6",
		"R": "7",
		"J": "8",
		"T": "9",
		"I": "10"
	];

	foreach( line; File(args[1], "r").byLine ) {
		auto arr = line.splitter(';').map!(x => x.chomp("\"").chompPrefix("\"")).array;

		if( arr.length < 3 )
			continue;

		if( arr[2] !in types )
			continue;

		outputFile.writeln( arr[1], ";", types[arr[2]] );
	}
}