#-------------------------------------------------
#
# Project created by QtCreator 2017-10-31T14:47:45
#
#-------------------------------------------------

QT       += core gui charts concurrent network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = SFC_projekt_xcejch00
TEMPLATE = app

Release:TARGET = xcejch00
Debug:TARGET = xcejch00_dbg

CONFIG += c++14

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
strawApi/Concurrent.cpp \
strawApi/LoadingSplashScreen.cpp \
gui/MainWindow.cpp \
main.cpp \
gui/NewConfigDialog.cpp \
rec/NeuralNetwork.cpp \
gui/NeuralNetworkPreviewWidget.cpp \
rec/ClassificationSample.cpp \
gui/ClassificationSamplesViewModel.cpp \
rec/WordClass.cpp \
etc/MurmurHash.cpp

HEADERS += \
strawApi/Concurrent.h \
strawApi/LoadingSplashScreen.h \
gui/MainWindow.h \
gui/NewConfigDialog.h \
rec/NeuralNetwork.h \
gui/NeuralNetworkPreviewWidget.h \
rec/ClassificationSample.h \
gui/ClassificationSamplesViewModel.h \
rec/WordClass.h \
etc/MurmurHash.h

FORMS += \
strawApi/LoadingSplashScreen.ui \
gui/MainWindow.ui \
gui/NewConfigDialog.ui


DESTDIR = ../bin

Release:OBJECTS_DIR = release/.obj
Release:MOC_DIR = release/.moc
Release:RCC_DIR = release/.rcc
Release:UI_DIR = release/.ui
Release:PRECOMPILED_DIR = release

Debug:OBJECTS_DIR = debug/.obj
Debug:MOC_DIR = debug/.moc
Debug:RCC_DIR = debug/.rcc
Debug:UI_DIR = debug/.ui
Debug:PRECOMPILED_DIR = debug

RESOURCES += \
../res/resource.qrc
