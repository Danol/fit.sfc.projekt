#ifndef MURMURHASH_H
#define MURMURHASH_H

#include <inttypes.h>

uint32_t murmurHash( const void * key, int len, uint32_t seed );

#endif // MURMURHASH_H
