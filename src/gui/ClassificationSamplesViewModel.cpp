#include "ClassificationSamplesViewModel.h"

#include <QColor>
#include <QIcon>

ClassificationSamplesViewModel::ClassificationSamplesViewModel()
{

}

void ClassificationSamplesViewModel::setDataSource(const QSharedPointer<ClassificationSampleLibrary> &dataSource)
{
	if( dataSource_ == dataSource )
		return;

	dataSource_ = dataSource;
	dataSourceList_ = dataSource ? &dataSource->wordsIndex : nullptr;
}

void ClassificationSamplesViewModel::setNeuralNetwork(const QSharedPointer<NeuralNetwork> &network)
{
	if( neuralNetwork_ == network )
		return;

	neuralNetwork_ = network;
}

void ClassificationSamplesViewModel::setExtDataAvailable(bool set)
{
	extDataAvailable_ = set;
}

void ClassificationSamplesViewModel::update()
{
	beginResetModel();
	endResetModel();
}

ClassificationSample *ClassificationSamplesViewModel::sampleAt(const QModelIndex &index)
{
	if( !dataSourceList_ || neuralNetwork_.isNull() )
		return nullptr;

	return (*dataSourceList_)[index.row()];
}

int ClassificationSamplesViewModel::rowCount(const QModelIndex &parent) const
{
	Q_UNUSED( parent );

	if( !dataSourceList_ || neuralNetwork_.isNull() )
		return 0;

	return dataSourceList_->size();
}

int ClassificationSamplesViewModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED( parent );

	return 4;
}

QVariant ClassificationSamplesViewModel::data(const QModelIndex &index, int role) const
{
	if( !dataSourceList_ || neuralNetwork_.isNull() )
		return QVariant();

	ClassificationSample &sample = *( (*dataSourceList_)[ index.row() ] );
	bool extDataAvailable = extDataAvailable_ && sample.netConfigurationId == neuralNetwork_->configurationId();

	if( role == Qt::DisplayRole ) {
		switch( index.column() ) {

		case 0:
			return sample.word();

		case 1:
			return tr("%1 – %2").arg( int( sample.wordClass() ) + 1 ).arg( wordClassNames[ sample.wordClass() ] );

		case 2:
			return neuralNetwork_->isSampleInTestSet( sample ) ? tr("T – testovací") : tr("R – trénovací");

		case 3:
			return extDataAvailable ? tr("%1 – %2").arg( int( sample.calculatedWordClass ) + 1 ).arg( wordClassNames[ sample.calculatedWordClass ] ) : "";

		default:
			return QVariant();

		}

	}	else if( role == Qt::TextColorRole ) {
		switch( index.column() ) {

		case 2: {
			static const QColor testSetColor(244, 179, 66), trainSetColor(66, 134, 244);
			return neuralNetwork_->isSampleInTestSet( sample ) ? testSetColor : trainSetColor;
		}

		case 3: {
			static const QColor okClassifiedColor(82, 203, 37), wrongClassifiedColor(200, 30, 30);
			return extDataAvailable ? QColor( sample.calculatedWordClass == sample.wordClass() ? okClassifiedColor : wrongClassifiedColor ) : Qt::black;
		}

		default:
			return QVariant();

		}
	}	else if( role == Qt::DecorationRole ) {
		switch( index.column() ) {

		case 3: {
			static const QIcon okIcon( ":/16/icons8_Checkmark_16px.png" ), wrongIcon( ":/16/icons8_Delete_16px.png" );
			return extDataAvailable ? ( sample.calculatedWordClass == sample.wordClass() ? okIcon : wrongIcon ) : QIcon();
		}

		default:
			return QVariant();

		}
	}

	return QVariant();
}

QVariant ClassificationSamplesViewModel::headerData(int section, Qt::Orientation orientation, int role) const
{
	if( role == Qt::DisplayRole ) {
		if( orientation == Qt::Horizontal ) {
			switch( section ) {

			case 0:
				return tr("Slovo");

			case 1:
				return tr("Slovní druh");

			case 2:
				return tr("Množina");

			case 3:
				return tr("Klasif. slovní druh");

			default:
				return QVariant();

			}
		}
	}

	return QVariant();
}
