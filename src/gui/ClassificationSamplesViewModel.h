#ifndef CLASSIFICATIONSAMPLESVIEWMODEL_H
#define CLASSIFICATIONSAMPLESVIEWMODEL_H

#include <QAbstractTableModel>
#include <QSharedPointer>
#include <QVector>

#include "rec/ClassificationSample.h"
#include "rec/NeuralNetwork.h"

class ClassificationSamplesViewModel : public QAbstractTableModel
{
	Q_OBJECT

public:
	ClassificationSamplesViewModel();

public:
	void setDataSource( const QSharedPointer<ClassificationSampleLibrary> &dataSource );
	void setNeuralNetwork( const QSharedPointer<NeuralNetwork> &network );
	void setExtDataAvailable( bool set );

	void update();

	ClassificationSample *sampleAt( const QModelIndex &index );

public:
	virtual int rowCount(const QModelIndex &parent = QModelIndex()) const override;
	virtual int columnCount(const QModelIndex &parent = QModelIndex()) const override;

	virtual QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;

private:
	QSharedPointer<ClassificationSampleLibrary> dataSource_;
	QSharedPointer<NeuralNetwork> neuralNetwork_;
	QList<ClassificationSample*> *dataSourceList_ = nullptr;

	/// Whether statistic & classification data for samples are available; they are not when the network is learning (to prevent race conditions as the learning is running on a separate thread)
	bool extDataAvailable_ = true;

};

#endif // CLASSIFICATIONSAMPLESVIEWMODEL_H
