#include "MainWindow.h"
#include "ui_MainWindow.h"

#include <math.h>
#include <QStringList>
#include <QThread>
#include <QRegExpValidator>
#include <QElapsedTimer>
#include <QMessageBox>
#include <QFileDialog>
#include <QtConcurrent/QtConcurrent>
#include <QAtomicInt>
#include <QNetworkReply>
#include <QNetworkAccessManager>
#include <QEventLoop>

#include "NewConfigDialog.h"
#include "strawApi/Concurrent.h"

const QColor chartColors[_wcCount+1] {
	QColor( 244, 67, 54 ),
	QColor( 156, 39, 176 ),
	QColor( 63, 81, 181 ),
	QColor( 109, 76, 65 ),
	QColor( 77, 208, 225 ),
	QColor( 56, 142, 60 ),
	QColor( 144, 164, 174 ),
	QColor( 255, 235, 59 ),
	QColor( 251, 140, 0 ),
	QColor( 30, 50, 56 ),
	QColor( 194, 255, 163 )
};

MainWindow *mainWindow;

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	splash = new LoadingSplashScreen( this );
	learningStopRequested_.reset( new bool );
	mainWindow = this;

	ui->wgtStack->setCurrentIndex( 0 );
	ui->twgtTop->setCurrentIndex( 0 );
	ui->lstSampleList->setModel( &samplesViewModel_ );
	ui->lnClassifyInput->setMaxLength( NeuralNetwork::maxInputStringLength );
	ui->lnClassifyInput->setValidator( new QRegExpValidator( QRegExp( QString("[%1]*").arg( NeuralNetwork::allowedInputSymbols ) ), this ) );

	connect( ui->lstSampleList->selectionModel(), SIGNAL(currentRowChanged(QModelIndex,QModelIndex)), this, SLOT(onSampleLibraryCurrentRowChanged(QModelIndex)) );

	auto commonChartSettings = [](QChart *ch) {
		ch->setBackgroundRoundness( 0 );
		ch->layout()->setContentsMargins( 4, 4, 4, 4 );
		ch->setMargins( QMargins( 8, 8, 8, 8 ) );
		ch->legend()->setAlignment( Qt::AlignRight );

		QFont titleFont = ch->titleFont();
		titleFont.setBold( true );
		ch->setTitleFont( titleFont );
	};

	// Learning chart
	{
		learningTrainingSetSuccessXSamplesLearnedSeries.setName( tr("R – Trénovací sada") );
		learningTestingSetSuccessXSamplesLearnedSeries.setName( tr("T – Testovací sada") );

		QChart &ch = learningSuccessXSamplesLearnedChart;
		ch.addSeries( &learningTrainingSetSuccessXSamplesLearnedSeries );
		ch.addSeries( &learningTestingSetSuccessXSamplesLearnedSeries );

		QValueAxis *aX = new QValueAxis();
		aX->setTitleText( tr("Naučeno vzorků") );
		aX->setTickCount( 17 );
		aX->setLabelFormat( "%i" );
		aX->setRange( 0, 1024 );

		QValueAxis *aY = new QValueAxis();
		aY->setTitleText( tr("Úspěšnost klasifikace") );
		aY->setRange( 0, 100 );
		aY->setTickCount( 11 );
		aY->setLabelFormat( "%i%" );

		ch.setAxisX( aX, &learningTrainingSetSuccessXSamplesLearnedSeries );
		ch.setAxisX( aX, &learningTestingSetSuccessXSamplesLearnedSeries );
		ch.setAxisY( aY, &learningTrainingSetSuccessXSamplesLearnedSeries );
		ch.setAxisY( aY, &learningTestingSetSuccessXSamplesLearnedSeries );

		commonChartSettings( &ch );
		ch.legend()->setAlignment( Qt::AlignTop );

		QChartView *chv = new QChartView(&learningSuccessXSamplesLearnedChart);
		chv->setRenderHint( QPainter::Antialiasing );

		QSizePolicy sp = chv->sizePolicy();
		sp.setHorizontalStretch( 3 );
		chv->setSizePolicy( sp );

		ui->ltTabLearning->addWidget( chv );
	}

	const QStringList setNames { tr("trénovací"), tr("testovací") };

	// Classification word class ratio chart
	for( int i = 0; i < _ssCount; i ++ ) {
		QChart *ch = new QChart();
		QPieSeries *ser = new QPieSeries(this);

		ch->addSeries( ser );
		ch->setTitle( tr("Rozložení vzorků v %1 množině").arg(setNames[i]) );

		commonChartSettings( ch );

		QChartView *chw = new QChartView( ch, this );
		chw->setRenderHint( QPainter::Antialiasing );

		QSizePolicy sp = chw->sizePolicy();
		sp.setHorizontalStretch( 1 );
		sp.setVerticalStretch( 1 );
		chw->setSizePolicy( sp );

		ui->ltStats->addWidget( chw, i+1, 0, 1, 1 );

		classificationWordClassesChart_[ i ] = ch;
		classificationWordClassesSeries_[ i ] = ser;
	}

	// Classification chart
	for( int i = 0; i < _ssCount; i ++ ) {
		QChart *ch = new QChart();
		QBarSeries *ser = new QBarSeries(this);

		QStringList categories;

		for( int wc = 0; wc < _wcCount; wc++ ) {
			categories << capitalizedWordClassNames[(WordClass) wc];

			QBarSet *set = new QBarSet(capitalizedWordClassNames[(WordClass) wc]);
			set->setColor( chartColors[wc] );

			for( int wc2 = 0; wc2 < _wcCount; wc2++ )
				set->append( 0.0001 );

			ser->append( set );
			classificationSets_[i][wc] = set;
		}

		ch->addSeries( ser );
		ch->createDefaultAxes();

		QBarCategoryAxis *aX = new QBarCategoryAxis();
		aX->append(categories);
		ch->setAxisX( aX, ser );

		QValueAxis *aY = new QValueAxis();
		aY->setRange( 0, 100 );
		aY->setLabelFormat( "%i%" );
		ch->setAxisY( aY, ser );

		ch->setTitle( tr("Třída určená neuronovou sítí v %1 množině").arg(setNames[i]) );
		commonChartSettings( ch );

		QChartView *chw = new QChartView( ch );
		chw->setRenderHint( QPainter::Antialiasing );

		QSizePolicy sp = chw->sizePolicy();
		sp.setHorizontalStretch( 3 );
		sp.setVerticalStretch( 1 );
		chw->setSizePolicy( sp );

		ui->ltStats->addWidget( chw, i+1, 2, 1, 1 );

		classificationChart_[ i ] = ch;
		classificationSeries_[ i ] = ser;
	}

	// Misclassification chart
	for( int i = 0; i < _ssCount; i ++ ) {
		QChart *ch = new QChart();
		QBarSeries *ser = new QBarSeries(this);

		for( int wc = 0; wc < _wcCount + 1; wc++ ) {
			QBarSet *set = new QBarSet( "" );
			set->setColor( chartColors[wc] );
			set->append( 0.0001 );

			ser->append( set );
			misclassificationSets_[i][wc] = set;
		}

		ch->addSeries( ser );

		QValueAxis *aY = new QValueAxis();
		aY->setRange( 0, 100 );
		aY->setLabelFormat( "%i%" );
		ch->setAxisY( aY, ser );

		ch->setTitle( tr("Úspěšnost klasifikace v %1 množině").arg(setNames[i]) );
		commonChartSettings( ch );

		QChartView *chw = new QChartView( ch );
		chw->setRenderHint( QPainter::Antialiasing );

		QSizePolicy sp = chw->sizePolicy();
		sp.setHorizontalStretch( 1 );
		sp.setVerticalStretch( 1 );
		chw->setSizePolicy( sp );

		ui->ltStats->addWidget( chw, i+1, 1, 1, 1 );

		misclassificationChart_[ i ] = ch;
		misclassificationSeries_[ i ] = ser;
	}
}

MainWindow::~MainWindow()
{
	*learningStopRequested_ = true;

	delete ui;
}

void MainWindow::init()
{
	// Load samples
	loadSamples();
}

void MainWindow::setNetwork(const QSharedPointer<NeuralNetwork> &net)
{
	if( net == network_ )
		return;

	network_ = net;
	ui->wgtNetPreview->setNetwork( net );
	samplesViewModel_.setNeuralNetwork( net );
	clearLearningStatistics();

	updateUI();
	onLearningStatisticsUpdate();
	updateConfigurationDependentElements();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
	*learningStopRequested_ = true;
	QMainWindow::closeEvent( event );
}

void MainWindow::updateUI()
{
	bool hasNetwork = !network_.isNull();

	ui->wgtNetPreview->setUpdatesEnabled( !runningAction_ );

	ui->actionClose->setEnabled( hasNetwork && !runningAction_ );
	ui->actionSave->setEnabled( hasNetwork && !runningAction_ );
	ui->actionLoad->setEnabled( !runningAction_ );
	ui->actionNew->setEnabled( !runningAction_ );
	ui->tabControl->setEnabled( !runningAction_ );
	ui->wgtLearningConfig->setEnabled( runningAction_ != raLearning );
	ui->tabClassification->setEnabled( !runningAction_ );
	ui->btnStartLearning->setEnabled( !runningAction_ && !sampleLibrary_.isNull() && !sampleLibrary_->baseList.isEmpty() );
	ui->btnStopLearning->setEnabled( runningAction_ == raLearning && !*learningStopRequested_ );

	ui->twgtTop->setTabEnabled( ui->twgtTop->indexOf( ui->tabStats ), !runningAction_ && !network_.isNull() && classificationStatistics_.configurationId == network_->configurationId() );
	ui->btnCalcStats->setEnabled( !runningAction_ );

	const QString runningActionText[_raCount] = { tr("Připraven"), tr("Probíhá učení"), tr("Probíhá statistické vyhodnocování") };
	const QPixmap runningActionIcon[_raCount] = { QPixmap(":/16/icons8_Checkmark_16px.png"), QPixmap(":/16/icons8_Class_16px.png"), QPixmap(":/16/icons8_Statistics_16px.png") };
	ui->lblRunningAction->setText( runningActionText[(int) runningAction_] );
	ui->lblRunningActionIcon->setPixmap( runningActionIcon[(int) runningAction_] );

	if( !hasNetwork ) {
		ui->wgtStack->setCurrentIndex( 0 );
		return;
	}

	ui->wgtStack->setCurrentIndex( 1 );

	ui->sbLearningCoef->setValue( network_->learningCoef );
	ui->cbUniformClassLearning->setChecked( network_->uniformClassLearning );

	samplesViewModel_.update();
	ui->wgtNetPreview->update();
}

void MainWindow::updateConfigurationDependentElements()
{
	ui->wgtNetPreview->updateImage();
	on_lnClassifyInput_textChanged( ui->lnClassifyInput->text() );
}

void MainWindow::clearLearningStatistics()
{
	learningStatistics_ = LearningStatistics();

	learningTrainingSetSuccessXSamplesLearnedSeries.clear();
	learningTestingSetSuccessXSamplesLearnedSeries.clear();
}

void MainWindow::loadSamples()
{
	static const QString downloadUrl = "https://gitlab.com/Danol/fit.sfc.projekt/raw/master/bin/data.csv";
	QPointer<MainWindow> thisPtr( this );

	if( !QFile("data.csv").exists() ) {
		if( QMessageBox::question( this, tr("Chybějící soubor dat"), tr("Chybí soubor dat '%1'. Stáhnout z git repozitáře?").arg( QDir(".").absoluteFilePath( "data.csv" ) ), QMessageBox::Yes | QMessageBox::Default, QMessageBox::No ) != QMessageBox::Yes )
			return;

		int splashId = splash->show( tr("Stahování databáze vzorků") );
		concurrent->runThreaded([=]{
			QNetworkAccessManager manager;
			QEventLoop loop;

			QNetworkReply *reply = manager.get( QNetworkRequest( downloadUrl ) );

			connect( &manager, SIGNAL(finished(QNetworkReply*)), &loop, SLOT(quit()) );
			loop.exec();

			if( reply->error() != QNetworkReply::NoError ) {
				reply->deleteLater();

				concurrent->runOnMainThread([=]{
					if( thisPtr.isNull() )
						return;

					QMessageBox::critical( this, tr("Chyba"), tr("Nepodařilo se stáhnout databázi vzorků: chyba při stahování") );
					splash->close( splashId );
				});

				return;
			}

			QByteArray data = reply->readAll();
			reply->deleteLater();

			QFile f( "data.csv" );
			if( !f.open(QIODevice::WriteOnly) ) {
				concurrent->runOnMainThread([=]{
					if( thisPtr.isNull() )
						return;

					QMessageBox::critical( this, tr("Chyba"), tr("Nepodařilo se zapsat do souboru '%1'").arg( QDir(".").absoluteFilePath("data.csv") ) );
					splash->close( splashId );
				});
				return;
			}

			f.write( data );
			concurrent->runOnMainThread([=]{
				if( thisPtr.isNull() )
					return;

				loadSamples();
				splash->close( splashId );
			});
		});
		return;
	}

	int splashId = splash->show( tr("Načítání vzorků") );
	concurrent->runThreaded([this, splashId] {
		QSharedPointer<ClassificationSampleLibrary> library = ClassificationSampleLibrary::load();
		concurrent->runOnMainThread([this, library, splashId]{
			if( library ) {
				sampleLibrary_ = library;
				samplesViewModel_.setDataSource( library );
				ui->lblSampleCount->setText( QString::number( library->baseList.length() ) );
			}

			splash->close( splashId );
			updateUI();
		});
	});
}

void MainWindow::updateStatistics()
{
	if( runningAction_ || sampleLibrary_.isNull() || network_.isNull() )
		return;

	runningAction_ = raClassifying;
	samplesViewModel_.setExtDataAvailable( false );
	updateUI();

	auto network = network_;
	auto sampleLibrary = sampleLibrary_;
	QPointer<MainWindow> thisPtr( this );

	concurrent->runThreadedDedicated([network, sampleLibrary, thisPtr, this]{
		const size_t configId = network->configurationId();

		ClassificationStatistics stats;
		QMutex statsAccessMutex;
		QAtomicInt sampleIndex = 0;
		QVector<QFuture<void>> futures;

		stats.configurationId = network->configurationId();
		stats.totalSamples = sampleLibrary->baseList.length();

		for( int thread = 0; thread < QThread::idealThreadCount(); thread ++ ) {
			futures.append( concurrent->runThreadedDedicated([&statsAccessMutex,&sampleIndex, &stats, &network, sampleLibrary, configId]{
				NeuralNetwork threadNetwork( *network );

				while( true ) {
					const int i = sampleIndex.fetchAndAddOrdered( 1 );
					if( i >= stats.totalSamples )
						break;

					ClassificationSample *sample = sampleLibrary->baseList[i];

					sample->calculatedWordClass = threadNetwork.classifyWord( sample->word() );
					sample->netConfigurationId = configId;

					const bool classifiedCorrectly = sample->calculatedWordClass == sample->wordClass();
					const bool isInTestingSet = threadNetwork.isSampleInTestSet( *sample );

					QMutexLocker ml(&statsAccessMutex);

					if( !classifiedCorrectly ) {
						stats.sampleInWordClasMissclassifications[isInTestingSet][(int) sample->wordClass()] ++;
						stats.misclassifiedSamples[isInTestingSet] ++;
					}

					stats.samples[isInTestingSet] ++;
					stats.samplesInWordClass[isInTestingSet][(int) sample->wordClass()] ++;
					stats.samplesClassifications[isInTestingSet][(int) sample->wordClass()][(int) sample->calculatedWordClass] ++;
				}
			}));
		}

		for( QFuture<void> &future : futures )
			future.waitForFinished();

		/*for( ClassificationSample *sample : sampleLibrary->baseList ) {
			sample->calculatedWordClass = network->classifyWord( sample->word() );
			sample->netConfigurationId = configId;

			const bool classifiedCorrectly = sample->calculatedWordClass == sample->wordClass();
			const bool isInTestingSet = network->isSampleInTestSet( *sample );

			if( !classifiedCorrectly )
				stats.sampleInWordClasMissclassifications[isInTestingSet][(int) sample->wordClass()] ++;

			stats.samples[isInTestingSet] ++;
			stats.samplesInWordClass[isInTestingSet][(int) sample->wordClass()] ++;
			stats.samplesClassifications[isInTestingSet][(int) sample->wordClass()][(int) sample->calculatedWordClass] ++;
		}*/

		concurrent->runOnMainThread([=]{
			if( thisPtr.isNull() )
				return;

			runningAction_ = raNone;
			classificationStatistics_ = stats;

			for( int i = 0; i < _ssCount; i ++ ) {
				QPieSeries *classificationWordClassesSeries = classificationWordClassesSeries_[ i ];
				classificationWordClassesSeries->clear();

				for( int wc = 0; wc < _wcCount; wc ++ ) {
					classificationWordClassesSeries->append( tr("%1 (%2)").arg( capitalizedWordClassNames[(WordClass) wc] ).arg( stats.samplesInWordClass[i][wc] ), stats.samplesInWordClass[i][wc] )->setColor( chartColors[wc] );

					const float misclassificationPercent = stats.samplesInWordClass[i][wc] ? ( stats.samplesInWordClass[i][wc] - stats.sampleInWordClasMissclassifications[i][wc] ) / (qreal) stats.samplesInWordClass[i][wc] * 100.0 : 0;
					misclassificationSets_[i][wc]->replace( 0, misclassificationPercent );
					misclassificationSets_[i][wc]->setLabel( tr("%1 (%2 %)").arg( capitalizedWordClassNames[(WordClass) wc] ).arg( int( misclassificationPercent ) ) );

					for( int wc2 = 0; wc2 < _wcCount; wc2 ++ )
						classificationSets_[i][wc2]->replace( wc, qMax<qreal>( 0.00001, stats.samplesInWordClass[i][wc] ? stats.samplesClassifications[i][wc][wc2] / (qreal)( stats.samplesInWordClass[i][wc] ) * 100.0 : 0 ) );
				}

				{
					const float misclassificationPercent = stats.misclassifiedSamples[i] ? ( stats.samples[i] - stats.misclassifiedSamples[i] ) / (qreal) stats.samples[i] * 100.0 : 0;
					misclassificationSets_[i][_wcCount]->replace( 0, misclassificationPercent );
					misclassificationSets_[i][_wcCount]->setLabel( tr("Celkem (%1 %)").arg( int( misclassificationPercent ) ) );
				}
			}

			samplesViewModel_.setExtDataAvailable( true );
			updateUI();
		});
	});
}

void MainWindow::updateStatChartsVisibility()
{
	static const int cols = 3, rows = 2;
	QPushButton *colButtons[cols] = { ui->btnShowStats_c0, ui->btnShowStats_c1, ui->btnShowStats_c2 };
	QPushButton *rowButtons[rows] = { ui->btnShowStats_r0, ui->btnShowStats_r1 };

	for( int col = 0; col < cols; col++ ) {
		for( int row = 0; row < rows; row++ ) {
			ui->ltStats->itemAtPosition( row + 1, col )->widget()->setVisible( colButtons[col]->isChecked() && rowButtons[row]->isChecked() );
		}
	}
}

void MainWindow::onSampleLibraryCurrentRowChanged(const QModelIndex &current)
{
	if( runningAction_ )
		return;

	QString word = samplesViewModel_.sampleAt( current )->word();

	// This is to prevent moving caret when not necessary
	if( ui->lnClassifyInput->text() != word )
		ui->lnClassifyInput->setText( word );
}

void MainWindow::onLearningStatisticsUpdate()
{
	ui->lblTestingSetSuccess->setText( tr("%1%").arg( int( learningStatistics_.testingSetSuccessRatio * 100 ) ) );
	ui->lblTrainingSetSuccess->setText( tr("%1%").arg( int( learningStatistics_.trainingSetSuccessRatio * 100 ) ) );
	ui->lblTrainedSamplesCount->setText( QString::number( network_->learnedSampleCount ) );
}

void MainWindow::on_actionNew_triggered()
{
	NewConfigDialog *dlg = new NewConfigDialog( this );
	dlg->setAttribute( Qt::WA_DeleteOnClose );
	dlg->show();
}

void MainWindow::on_actionClose_triggered()
{
	network_.clear();
	updateUI();
}

void MainWindow::on_lnClassifyInput_textChanged(const QString &arg1)
{
	if( runningAction_ )
		return;

	static const QColor okClassifiedColor(82, 203, 37), wrongClassifiedColor(200, 30, 30), neutralColor = Qt::black;

	WordClass class_ = network_->classifyWord( arg1 );
	ui->lblClassifyOutput->setText( wordClassNames[class_] );
	ui->wgtNetPreview->updateImage();
	ui->twgtTop->setCurrentIndex( 0 );

	QPalette palette = ui->lblClassifyOutput->palette();
	palette.setColor( QPalette::Foreground, neutralColor );

	if( !sampleLibrary_.isNull() ) {
		ClassificationSample *sample = sampleLibrary_->words.value( arg1, nullptr );
		if( sample ) {
			sample->calculatedWordClass = class_;
			sample->netConfigurationId = network_->configurationId();

			bool classifiedCorrectly = sample->calculatedWordClass == sample->wordClass();

			ui->lstSampleList->setCurrentIndex( samplesViewModel_.index( sample->indexIn(), 0 ) );
			palette.setColor( QPalette::Foreground, classifiedCorrectly ? okClassifiedColor : wrongClassifiedColor );
		}
	}

	ui->lblClassifyOutput->setPalette( palette );
}

void MainWindow::on_cmbUpdateWeights_currentIndexChanged(int index)
{
	if( index < 0 )
		return;

	switch( index ) {

	case 1:
		network_->zeroWeights();
		break;

	case 2:
		network_->randomizeWeights();
		break;

	case 3:
		network_->randomizeFirstLayerWeights_OtherSetToIdentity();
		break;

	}

	updateConfigurationDependentElements();
	clearLearningStatistics();
	ui->cmbUpdateWeights->setCurrentIndex( 0 );
}

void MainWindow::on_sbLearningCoef_valueChanged(double arg1)
{
	network_->learningCoef = arg1;
}

const float MainWindow::LearningStatistics::slidingAvgCoef = 0.0005f;

void MainWindow::on_btnStartLearning_clicked()
{
	Q_ASSERT(!runningAction_);

	runningAction_ = raLearning;
	*learningStopRequested_ = false;
	samplesViewModel_.setExtDataAvailable( false );
	updateUI();

	QPointer<MainWindow> thisPtr( this );
	auto network = network_;
	auto learningStopRequested = learningStopRequested_;
	auto stats_ = learningStatistics_;
	auto sampleLibrary = sampleLibrary_;
	auto stopAfter = ui->sbStopLearningAfter->value();

	concurrent->runThreadedDedicated([this, thisPtr, network, learningStopRequested, stats_, sampleLibrary, stopAfter]{
		QElapsedTimer tmr;
		tmr.start();
		int newlyTrainedSamples = 0, trainedSamplesThisRun = 0;

		LearningStatistics stats = stats_;

		auto sendStats = [this, &thisPtr, &stats, &newlyTrainedSamples, network]{
			concurrent->runOnMainThread([=]{
				if( thisPtr.isNull() )
					return;

				learningStatistics_ = stats;
				network->learnedSampleCount += newlyTrainedSamples;

				auto compressSeries = [](QLineSeries &series){
					QVector<QPointF> points = series.pointsVector();

					while( points.size() > 512 ) {
						for( int i = 0; i < (points.size()-1) / 2; i ++ )
							points[i] = QPointF( points[i*2].x(), points[i*2].y() * 0.5 + points[i*2+1].y() * 0.5 );

						points.resize( points.size() / 2 );
					}

					series.replace( points );
				};

				compressSeries(learningTrainingSetSuccessXSamplesLearnedSeries);
				compressSeries(learningTestingSetSuccessXSamplesLearnedSeries);

				learningTrainingSetSuccessXSamplesLearnedSeries.append( network->learnedSampleCount, stats.trainingSetSuccessRatio * 100 );
				learningTestingSetSuccessXSamplesLearnedSeries.append( network->learnedSampleCount, stats.testingSetSuccessRatio * 100 );
				learningSuccessXSamplesLearnedChart.axisX()->setRange( 0, qMax<int>( 1024, pow( 2, ceil( log2( network->learnedSampleCount ) ) ) ) );

				onLearningStatisticsUpdate();

			});
		};

		// We learn in stochastic mode
		while( !*learningStopRequested_ && sampleLibrary->baseList.length() && ( !stopAfter || trainedSamplesThisRun < stopAfter ) ) {
			ClassificationSample *sample;
			if( network->uniformClassLearning ) {
				const int wordClass = qrand() % _wcCount;
				const int dataSize = sampleLibrary->classLists[wordClass].length();

				if( dataSize == 0 )
					continue;

				sample = sampleLibrary->classLists[wordClass][ qrand() % dataSize ];

			} else {
				const int dataSize = sampleLibrary->baseList.length();

				if( dataSize == 0 )
					break;

				sample = sampleLibrary->baseList[ qrand() % dataSize ];
			}

			network->prepareLearning();

			if( network->isSampleInTestSet( *sample ) ) {
				stats.testingSetSuccessRatio =
						( network->classifyWord( sample->word() ) == sample->wordClass() ? 1.0f : 0.0f ) * LearningStatistics::slidingAvgCoef
						+ stats.testingSetSuccessRatio * ( 1.0f - LearningStatistics::slidingAvgCoef );

			} else {
				stats.trainingSetSuccessRatio =
						( network->learnSample( *sample ) ? 1.0f : 0.0f ) * LearningStatistics::slidingAvgCoef
						+ stats.trainingSetSuccessRatio * ( 1.0f - LearningStatistics::slidingAvgCoef );
			}

			network->applyLearning();
			newlyTrainedSamples ++;
			trainedSamplesThisRun ++;

			if( tmr.elapsed() > 250 ) {
				sendStats();
				tmr.restart();
				newlyTrainedSamples = 0;
			}
		}

		sendStats();
		concurrent->runOnMainThread([=]{
			if( thisPtr.isNull() )
				return;

			runningAction_ = raNone;
			learningStatistics_ = stats;
			samplesViewModel_.setExtDataAvailable( true );

			updateUI();

			ui->wgtNetPreview->updateImage();
		});
	});
}

void MainWindow::on_btnStopLearning_clicked()
{
	*learningStopRequested_ = true;
	updateUI();
}

void MainWindow::on_actionSave_triggered()
{
	QFileDialog dlg( this );
	dlg.setFileMode( QFileDialog::AnyFile );
	dlg.setDefaultSuffix( "xcejch00_sfc" );
	dlg.setWindowTitle( tr( "Uložení sítě" ) );
	dlg.setNameFilter( tr( "Konfigurace sítě (*.xcejch00_sfc)" ) );
	dlg.setWindowIcon( QIcon( ":/16/icons8_Save_16px.png" ) );
	dlg.setAcceptMode( QFileDialog::AcceptSave );

	if( !dlg.exec() )
		return;

	QString fileName = dlg.selectedFiles().first();
	QFile f( fileName );
	if( !f.open( QIODevice::WriteOnly ) ) {
		QMessageBox::critical( this, tr( "Chyba" ), tr( "Nepodařilo se zapsat do souboru." ) );
		return;
	}

	f.write( QJsonDocument( network_->toJson() ).toBinaryData() );
	QMessageBox::information( this, tr( "Uloženo" ), tr( "Neurální síť byla uložena do souboru '%1'." ).arg( fileName ) );
}

void MainWindow::on_actionLoad_triggered()
{
	QFileDialog dlg( this );
	dlg.setFileMode( QFileDialog::AnyFile );
	dlg.setDefaultSuffix( "xcejch00_sfc" );
	dlg.setWindowTitle( tr( "Načtení konfigurace" ) );
	dlg.setNameFilter( tr( "Konfigurace sítě (*.xcejch00_sfc)" ) );
	dlg.setWindowIcon( QIcon( ":/16/icons8_Open_16px.png" ) );
	dlg.setAcceptMode( QFileDialog::AcceptOpen );

	if( !dlg.exec() )
		return;

	QString fileName = dlg.selectedFiles().first();
	QFile f( fileName );
	if( !f.open( QIODevice::ReadOnly ) ) {
		QMessageBox::critical( this, tr( "Chyba" ), tr( "Nepodařilo se otevřít soubor." ) );
		return;
	}

	QByteArray rawData = f.readAll();
	QJsonObject data = QJsonDocument::fromBinaryData( rawData ).object();

	if( data.isEmpty() ) {
		QMessageBox::critical( this, tr( "Chyba" ), tr( "Formát dat v souboru není validní." ) );
		return;
	}

	NeuralNetwork *net = NeuralNetwork::createFromJson( data );
	if( !net ) {
		QMessageBox::critical( this, tr( "Chyba" ), tr( "Soubor neobsahuje validní konfiguraci neuronové sítě." ) );
		return;
	}

	setNetwork( QSharedPointer<NeuralNetwork>( net ) );
}

void MainWindow::on_cbUniformClassLearning_toggled(bool checked)
{
	if( !network_ )
		return;

	network_->uniformClassLearning = checked;
}

void MainWindow::on_btnCalcStats_clicked()
{
	updateStatistics();
}
