#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTimer>
#include <QtCharts>

#include "strawApi/LoadingSplashScreen.h"
#include "rec/NeuralNetwork.h"
#include "rec/ClassificationSample.h"
#include "gui/ClassificationSamplesViewModel.h"

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

	enum SampleSet {
		ssTraining,
		ssTesting,
		_ssCount
	};

	enum RunningAction {
		raNone,
		raLearning,
		raClassifying,
		_raCount
	};

	struct LearningStatistics {
		static const float slidingAvgCoef;
		float trainingSetSuccessRatio = 0.0f;
		float testingSetSuccessRatio = 0.0f;
	};

	struct ClassificationStatistics {
		int totalSamples = 0;

		/// How many samples in each set
		int samples[_ssCount] = {0};

		/// How many misclassified samples in each set
		int misclassifiedSamples[_ssCount] = {0};

		/// How many samples in each class
		int samplesInWordClass[_ssCount][_wcCount] = {{0}};

		/// How many missclassified samples in each class
		int sampleInWordClasMissclassifications[_ssCount][_wcCount] = {{0}};

		/// What classes are samples from each class classified to (by the network)
		int samplesClassifications[_ssCount][_wcCount][_wcCount] = {{{0}}};

		size_t configurationId = 0;
	};

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	void init();

public:
	void setNetwork( const QSharedPointer<NeuralNetwork> &net );

protected:
	void closeEvent(QCloseEvent *event) override;

private:
	void updateUI();
	void updateConfigurationDependentElements();
	void clearLearningStatistics();

	/// Loads samples from data CSV file
	void loadSamples();

	/// Classifies all samples and generates classification statistics
	void updateStatistics();

private slots:
	void updateStatChartsVisibility();

public:
	LoadingSplashScreen *splash;

private:
	QSharedPointer<NeuralNetwork> network_;
	QSharedPointer<ClassificationSampleLibrary> sampleLibrary_;
	ClassificationSamplesViewModel samplesViewModel_;

private:
	RunningAction runningAction_ = raNone;
	QSharedPointer<bool> learningStopRequested_;
	LearningStatistics learningStatistics_;

private:
	QChart learningSuccessXSamplesLearnedChart;
	QLineSeries learningTrainingSetSuccessXSamplesLearnedSeries, learningTestingSetSuccessXSamplesLearnedSeries;

private:
	ClassificationStatistics classificationStatistics_;
	QChart *classificationWordClassesChart_[_ssCount];
	QPieSeries *classificationWordClassesSeries_[_ssCount];

	QChart *classificationChart_[_ssCount];
	QBarSeries *classificationSeries_[_ssCount];
	QBarSet *classificationSets_[_ssCount][_wcCount];

	QChart *misclassificationChart_[_ssCount];
	QBarSeries *misclassificationSeries_[_ssCount];
	QBarSet *misclassificationSets_[_ssCount][_wcCount+1];

private:
	Ui::MainWindow *ui;

private slots:
	void onSampleLibraryCurrentRowChanged(const QModelIndex &current);
	void onLearningStatisticsUpdate();

private slots:
	void on_actionNew_triggered();
	void on_actionClose_triggered();
	void on_lnClassifyInput_textChanged(const QString &arg1);
	void on_cmbUpdateWeights_currentIndexChanged(int index);
	void on_sbLearningCoef_valueChanged(double arg1);
	void on_btnStartLearning_clicked();
	void on_btnStopLearning_clicked();
	void on_actionSave_triggered();
	void on_actionLoad_triggered();
	void on_cbUniformClassLearning_toggled(bool checked);
	void on_btnCalcStats_clicked();
};

extern MainWindow *mainWindow;

#endif // MAINWINDOW_H
