#include "NeuralNetworkPreviewWidget.h"

#include <QPainter>
#include <QtConcurrent/QtConcurrent>
#include <QPainterPath>

NeuralNetworkPreviewWidget::NeuralNetworkPreviewWidget(QWidget *parent) :
	QWidget(parent)
{
	connect( &updateCacheWatcher_, SIGNAL(finished()), this, SLOT(onImageUpdateFinished()) );

	threadedCancelRequest_ = QSharedPointer<bool>( new bool() );
	*threadedCancelRequest_ = false;
}

void NeuralNetworkPreviewWidget::setNetwork(const QSharedPointer<NeuralNetwork> &net)
{
	if( net == network_ )
		return;

	network_ = net;
	imageCacheCanvasSize_ = QSize();
	networkConfigurationId_ = 0;

	update();
}

void NeuralNetworkPreviewWidget::setUpdatesEnabled(bool set)
{
	if( updatesEnabled_ == set )
		return;

	updatesEnabled_ = set;
	if( set )
		update();
}

void NeuralNetworkPreviewWidget::updateImage()
{
	imageUpdateRequired_ = true;
	*threadedCancelRequest_ = true;
	update();
}

void NeuralNetworkPreviewWidget::paintEvent(QPaintEvent *e)
{
	if( !isVisible() )
		return;

	Q_UNUSED( e );
	Q_ASSERT( cache_.nodesLayer_.size() == cache_.weightsLayer_.size() );

	QPainter p(this);
	p.setRenderHint( QPainter::SmoothPixmapTransform );

	QRect drawRect;
	drawRect.setSize( cache_.nodesLayer_.size().scaled( size(), Qt::KeepAspectRatio ) );
	drawRect.moveCenter( rect().center() );
	p.drawImage( drawRect, cache_.weightsLayer_ );
	p.drawImage( drawRect, cache_.nodesLayer_ );
	p.drawImage( drawRect, cache_.labelsLayer_ );

	if( imageCacheCanvasSize_ != size() || networkConfigurationId_ != network_->configurationId() || imageUpdateRequired_ )
		updateImageImpl();

	if( imageIsUpdating_ || !updatesEnabled_ )
		p.fillRect( rect(), QColor( 0, 0, 0, 100 ) );
}

void NeuralNetworkPreviewWidget::updateImageImpl()
{
	if( imageIsUpdating_ || !updatesEnabled_ ) {
		imageUpdateRequired_ = true;
		return;
	}

	imageIsUpdating_ = true;
	imageUpdateRequired_ = false;
	*threadedCancelRequest_ = false;

	// Create a copy of the network
	QSharedPointer<NeuralNetwork> network( new NeuralNetwork(*network_) );
	QSize canvasSize = size();

	auto threadedCancelRequest = threadedCancelRequest_;
	bool weightsLayerUpdateRequired = ( imageCacheCanvasSize_ != canvasSize ) || ( networkConfigurationId_ != network->configurationId() );
	bool labelsLayerUpdateRequired = ( imageCacheCanvasSize_ != canvasSize );

	auto future = QtConcurrent::run([network, canvasSize, threadedCancelRequest, weightsLayerUpdateRequired, labelsLayerUpdateRequired]{
		static const int inputLayerColmns = NeuralNetwork::symbolCount;
		static const int inputLayerColumnSize = 3;

		Cache result;
		result.networkConfigurationId = network->configurationId();
		result.canvasSize = canvasSize;

		QSizeF neuronSpacing( canvasSize.width() / float( network->layerCount() + inputLayerColumnSize ), canvasSize.height() / qMax<float>( network->maxLayerNeuronCount(), NeuralNetwork::inputLayerSize / inputLayerColmns ) );

		float neuronSizeDimension = qMin( neuronSpacing.width(), neuronSpacing.height() );
		QSizeF neuronSize = QSizeF( neuronSizeDimension, neuronSizeDimension ) * 0.7f;
		QSizeF inputSize = neuronSize * 0.5f;

		QVector<QVector<QPointF>> nodePositions;
		nodePositions.resize( network->layerCount() + 1 );

		// First we calculate neuron positions
		{
			float x = ( canvasSize.width() - neuronSpacing.width() * ( network->layerCount() + inputLayerColumnSize - 1 ) ) / 2.0f;

			// Because input layer is big, we display the nodes in zig-zag
			{
				auto &list = nodePositions[0];
				list.resize( NeuralNetwork::inputLayerSize );

				float y = ( canvasSize.height() - neuronSpacing.height() * ( ( list.size() - 1 ) / inputLayerColmns ) ) / 2.0f;

				for( int j = 0; j < list.size(); j ++ ) {
					list[j] = QPointF( x + neuronSpacing.width() * (j % inputLayerColmns) * ( inputLayerColumnSize - 1 ) / float(inputLayerColmns), y + neuronSpacing.height() * ( j / inputLayerColmns ) );
				}

				x += neuronSpacing.width() * inputLayerColumnSize;
			}

			for( int i = 1; i < nodePositions.size(); i++ ) {
				auto &list = nodePositions[i];
				list.resize( network->layerSize( i - 1 )  );

				float y = ( canvasSize.height() - neuronSpacing.height() * ( list.size() - 1 ) ) / 2.0f;

				for( auto &nodePos : list ) {
					nodePos = QPointF( x, y );
					y += neuronSpacing.height();
				}

				x += neuronSpacing.width();
			}
		}

		// Draw weights
		if( weightsLayerUpdateRequired ) {
			result.weightsLayer_ = QImage( canvasSize, QImage::Format_ARGB32 );
			result.weightsLayer_.fill( Qt::transparent );

			QPainter p(&result.weightsLayer_);
			p.setRenderHint( QPainter::Antialiasing );

			QColor activatingWeightColor( 0, 0, 0 );
			QColor prohibitingWeightColor( 200, 0, 0 );

			QPen pen( Qt::black, 1 );

			for( int layer = 0; layer < network->layerCount(); layer++ ) {
				const int weightCount = network->layerWeightCount(layer);

				for( int neuron = 0; neuron < network->layerSize(layer); neuron++ ) {
					float weightSum = 0.001f;
					for( int weight = 0; weight < weightCount; weight++ )
						weightSum += abs( network->neuronWeight( layer, neuron, weight ) );

					for( int weight = 0; weight < weightCount; weight++ ) {
						float alpha = network->neuronWeight( layer, neuron, weight ) / weightSum;

						if( alpha < 0 ) {
							prohibitingWeightColor.setAlphaF( qBound( 0.0f, -alpha, 1.0f ) );
							pen.setColor( prohibitingWeightColor );
						} else {
							activatingWeightColor.setAlphaF( qBound( 0.0f, alpha, 1.0f ) );
							pen.setColor( activatingWeightColor );
						}

						p.setPen( pen );
						p.drawLine( nodePositions[layer][weight], nodePositions[layer+1][neuron] );
					}

					if( *threadedCancelRequest )
						return Cache();
				}
			}
		}

		// Draw neurons
		{
			result.nodesLayer_ = QImage( canvasSize, QImage::Format_ARGB32 );
			result.nodesLayer_.fill( Qt::transparent );

			QPen basicPen( Qt::black, 1 );
			QPen activeOutputNeuronPen( Qt::blue, 3 );

			QPainter p(&result.nodesLayer_);
			p.setRenderHint( QPainter::Antialiasing );

			p.setBrush( Qt::white );
			p.setPen( basicPen );

			const QColor activeInputColor( 229, 83, 0 );

			// Input layer is smaller
			{
				for( int input = 0; input < NeuralNetwork::inputLayerSize; input ++ ) {
					p.setBrush( network->inputValue( input ) ? activeInputColor : Qt::white );
					p.drawEllipse( nodePositions[0][input], inputSize.width() * 0.5f, inputSize.height() * 0.5f );
				}
			}

			const QColor activeNeuronColor( 0, 200, 0 );

			for( int layer = 0; layer < network->layerCount(); layer ++ ) {
				auto &positions = nodePositions[layer + 1];

				for( int neuron = 0; neuron < network->layerSize( layer ); neuron ++ ) {
					float value = network->neuronValue( layer, neuron );

					p.setPen( Qt::transparent );
					p.setBrush( Qt::white );
					p.drawEllipse( positions[neuron], neuronSize.width() / 2, neuronSize.height() / 2 );

					p.setBrush( activeNeuronColor );
					p.setPen( Qt::transparent );
					p.drawPie(
								positions[neuron].x() - neuronSize.width() * 0.5f,
								positions[neuron].y() - neuronSize.height() * 0.5f,
								neuronSize.width(),
								neuronSize.height(),
								0,
								value * 360 * 16
								);

					p.setBrush( Qt::transparent );
					p.setPen( ( layer != network->layerCount() - 1 || neuron != network->activeOutput() ) ? basicPen : activeOutputNeuronPen );
					p.drawEllipse( positions[neuron], neuronSize.width() / 2, neuronSize.height() / 2 );
				}

				if( *threadedCancelRequest )
					return Cache();
			}
		}

		// Draw labels
		if( labelsLayerUpdateRequired ) {
			result.labelsLayer_ = QImage( canvasSize, QImage::Format_ARGB32 );
			result.labelsLayer_.fill( Qt::transparent );

			QPainter p(&result.labelsLayer_);
			p.setRenderHint( QPainter::Antialiasing );

			QFont fnt( "Tahoma" );
			fnt.setPixelSize( qMax<int>( inputSize.width(), 1 ) );

			QFontMetrics metrics( fnt, p.device() );
			QPainterPath path;

			for( int input = 0; input < NeuralNetwork::inputLayerSize; input ++ ) {
				const int index = input % ( NeuralNetwork::allowedInputSymbols.length() + 1);
				QString text = ( index == 0 ) ? QString("") : NeuralNetwork::allowedInputSymbols[index - 1];
				QRect textRect = metrics.boundingRect( text );

				path.addText( nodePositions[0][input] + QPointF( -textRect.width() / 2, -inputSize.height() / 2 - textRect.height() ) - textRect.topLeft(), fnt, text );
			}

			for( int output = 0; output < _wcCount; output ++ ) {
				QString text = wordClassNames[(WordClass) output];
				QRect textRect = metrics.boundingRect( text );

				path.addText( nodePositions[network->layerCount()][output] + QPointF( neuronSize.width(), -textRect.height() / 2 ) - textRect.topLeft(), fnt, text );
			}

			p.setBrush( Qt::black );
			p.setPen( QPen( Qt::white, 4 ) );
			p.drawPath( path );

			p.setPen( Qt::transparent );
			p.drawPath( path );
		}

		return result;
	});

	updateCacheWatcher_.setFuture( future );
}

void NeuralNetworkPreviewWidget::onImageUpdateFinished()
{
	imageIsUpdating_ = false;
	*threadedCancelRequest_ = false;

	Cache result = updateCacheWatcher_.result();

	imageCacheCanvasSize_ = result.canvasSize;
	networkConfigurationId_ = result.networkConfigurationId;

	if( !result.nodesLayer_.isNull() )
		cache_.nodesLayer_ = result.nodesLayer_;

	if( !result.weightsLayer_.isNull() )
		cache_.weightsLayer_ = result.weightsLayer_;

	if( !result.labelsLayer_.isNull() )
		cache_.labelsLayer_ = result.labelsLayer_;

	update();
}
