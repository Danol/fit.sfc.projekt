#ifndef NEURALNETWORKPREVIEWWIDGET_H
#define NEURALNETWORKPREVIEWWIDGET_H

#include <QWidget>
#include <QFutureWatcher>

#include "rec/NeuralNetwork.h"

class NeuralNetworkPreviewWidget : public QWidget
{
	Q_OBJECT

	struct Cache {
		/// Image with weights - this does not change until weights change
		QImage weightsLayer_;

		/// Image with nodes (inputs + neurons) - this changes when values change
		QImage nodesLayer_;

		QImage labelsLayer_;

		size_t networkConfigurationId;
		QSize canvasSize;
	};

public:
	explicit NeuralNetworkPreviewWidget(QWidget *parent = nullptr);

public:
	void setNetwork( const QSharedPointer<NeuralNetwork> &net );
	void setUpdatesEnabled( bool set );
	void updateImage();

protected:
	void paintEvent(QPaintEvent *e);

private:
	void updateImageImpl();

private slots:
	void onImageUpdateFinished();

private:
	QSharedPointer<NeuralNetwork> network_;
	QFutureWatcher<Cache> updateCacheWatcher_;
	bool imageIsUpdating_ = false, imageUpdateRequired_ = false;
	size_t networkConfigurationId_;
	Cache cache_;
	QSize imageCacheCanvasSize_;
	bool updatesEnabled_ = true;
	QSharedPointer<bool> threadedCancelRequest_;

};

#endif // NEURALNETWORKPREVIEWWIDGET_H
