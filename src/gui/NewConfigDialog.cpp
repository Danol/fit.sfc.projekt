#include "NewConfigDialog.h"
#include "ui_NewConfigDialog.h"

#include "MainWindow.h"

NewConfigDialog::NewConfigDialog(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::NewConfigDialog)
{
	ui->setupUi(this);
	updateNeuronsInLayerCntSpinboxes();
	on_sldTestSetPercentage_valueChanged( ui->sldTestSetPercentage->value() );
}

NewConfigDialog::~NewConfigDialog()
{
	delete ui;
}

void NewConfigDialog::updateNeuronsInLayerCntSpinboxes()
{
	int cnt = ui->sbLayerCount->value();
	while( neuronsInLayerSpinboxes.length() > cnt )
		delete neuronsInLayerSpinboxes.takeLast();

	while( neuronsInLayerSpinboxes.length() < cnt ) {
		QSpinBox *sbox = new QSpinBox( this );
		sbox->setMinimum( 1 );
		sbox->setMaximum( NeuralNetwork::maxLayerSize );
		sbox->setValue( 16 );

		ui->ltNeuronInLayerCnt->addWidget( sbox );
		neuronsInLayerSpinboxes.append( sbox );
	}
}

void NewConfigDialog::on_sbLayerCount_valueChanged(int arg1)
{
	Q_UNUSED( arg1 );
	updateNeuronsInLayerCntSpinboxes();
}

void NewConfigDialog::on_btnOk_clicked()
{
	QVector<int> neuronInLayerCount;
	for( QSpinBox* sb : neuronsInLayerSpinboxes )
		neuronInLayerCount.append( sb->value() );

	NeuralNetwork *network = new NeuralNetwork( neuronInLayerCount, ui->sldTestSetPercentage->value() );
	network->randomizeWeights();
	mainWindow->setNetwork( QSharedPointer<NeuralNetwork>( network ) );
	accept();
}

void NewConfigDialog::on_sldTestSetPercentage_valueChanged(int value)
{
	ui->lblTrainPercentage->setText( tr("%1% trénovací").arg(100 - value) );
	ui->lblTestPercentage->setText( tr("%1% testovací").arg(value) );
}
