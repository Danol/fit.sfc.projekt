#ifndef NEWCONFIGDIALOG_H
#define NEWCONFIGDIALOG_H

#include <QDialog>
#include <QSpinBox>

namespace Ui {
	class NewConfigDialog;
}

class NewConfigDialog : public QDialog
{
	Q_OBJECT

public:
	explicit NewConfigDialog(QWidget *parent = 0);
	~NewConfigDialog();

private:
	void updateNeuronsInLayerCntSpinboxes();

private slots:
	void on_sbLayerCount_valueChanged(int arg1);

	void on_btnOk_clicked();

	void on_sldTestSetPercentage_valueChanged(int value);

private:
	Ui::NewConfigDialog *ui;
	QList<QSpinBox*> neuronsInLayerSpinboxes;

};

#endif // NEWCONFIGDIALOG_H
