#include <QApplication>

#include <time.h>

#include "gui/MainWindow.h"
#include "strawApi/Concurrent.h"

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	qsrand( time(0) );

	concurrent = new Concurrent();

	MainWindow w;
	w.splash->setStartupMode( true );
	int splashId = w.splash->show( QObject::tr("Spouštění aplikace") );

	w.showMaximized();
	w.init();

	// Wait for the all loading background tasks to finish
	concurrent->waitWithProcessingEvents();
	w.splash->setStartupMode( false );
	w.splash->close( splashId );

	int result = a.exec();
	QThreadPool::globalInstance()->waitForDone();

	return result;
}
