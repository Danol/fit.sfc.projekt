#include "ClassificationSample.h"

#include <QFile>
#include <QMessageBox>
#include <QDir>

#include "gui/MainWindow.h"
#include "strawApi/Concurrent.h"
#include "etc/MurmurHash.h"

ClassificationSample::ClassificationSample(const QString &word, WordClass wordClass)
{
	word_ = word.toLower();
	wordClass_ = wordClass;
	wordHash_ = murmurHash( word.data(), sizeof(QChar) * word.length(), 27015 );
}

QSharedPointer<ClassificationSampleLibrary> ClassificationSampleLibrary::load()
{
	QFile samplesFile( "data.csv" );
	if( !samplesFile.open( QIODevice::ReadOnly ) ) {
		concurrent->runOnMainThread([=]{
			QMessageBox::critical( mainWindow, QObject::tr("Chyba"), QObject::tr("Nepodařilo se otevřít soubor s daty '%1'").arg( QDir(".").absoluteFilePath("data.csv") ) );
		});
		return nullptr;
	}

	QSharedPointer<ClassificationSampleLibrary> result( new ClassificationSampleLibrary );
	QRegExp allowedInputRegex( QString("[%1]*").arg( NeuralNetwork::allowedInputSymbols ) );

	while( samplesFile.bytesAvailable() ) {
		QStringList data = QString::fromUtf8( samplesFile.readLine() ).split(';');
		if( data.length() != 2 )
			continue;

		int class_ = data[1].toInt() - 1;
		if( class_ < 0 || class_ >= _wcCount )
			continue;

		QString word = data[0].toLower();
		if( word.length() > NeuralNetwork::maxInputStringLength )
			continue;

		if( result->words.contains( word ) )
			continue;

		if( !allowedInputRegex.exactMatch( word ) )
			continue;

		ClassificationSample *sample = new ClassificationSample( word, (WordClass) ( class_ ) );
		result->words.insert( word, sample );
		result->baseList.append( sample );
		result->classLists[class_].append( sample );
	}

	// Construct words index
	{
		auto tmpLst = result->words.values();
		result->wordsIndex.swap( tmpLst );
		for( int i = 0; i < result->wordsIndex.length(); i ++ )
			result->wordsIndex[i]->indexIn_ = i;
	}

	return result;
}
