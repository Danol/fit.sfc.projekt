#ifndef CLASSIFICATIONSAMPLE_H
#define CLASSIFICATIONSAMPLE_H

#include <QString>
#include <QMap>
#include <QSharedPointer>
#include <QVector>
#include <QHash>

#include "rec/WordClass.h"

class ClassificationSample
{
	friend class ClassificationSampleLibrary;

public:
	ClassificationSample( const QString &word, WordClass wordClass );

public:
	const QString& word() const {
		return word_;
	}

	WordClass wordClass() const {
		return wordClass_;
	}

	uint32_t wordHash() const {
		return wordHash_;
	}

	size_t indexIn() const {
		return indexIn_;
	}

public:
	/// Calculated by the network
	WordClass calculatedWordClass;

	/// ConfigurationID of the network in the time this sample was classified; if it does not equal to the current network classification id, the classification is invalid
	size_t netConfigurationId = 0;

private:
	QString word_;
	WordClass wordClass_;

	/// How the neural network classified this word
	WordClass neuralClassification_;

	/// Murmur hash of the word
	uint32_t wordHash_;

	/// Index in various lists (currently only wordsIndex)
	size_t indexIn_;

};

class ClassificationSampleLibrary {

public:
	static QSharedPointer<ClassificationSampleLibrary> load();

	~ClassificationSampleLibrary() {
		qDeleteAll( baseList );
	}

public:
	/// Basic unordered list
	QVector<ClassificationSample*> baseList;

	/// Container for fast lookup by words
	QMap<QString, ClassificationSample*> words;

	/// Container containing samples sorted by word
	QList<ClassificationSample*> wordsIndex;

	/// Samples grouped by class
	QVector<ClassificationSample*> classLists[_wcCount];

};

#endif // CLASSIFICATIONSAMPLE_H
