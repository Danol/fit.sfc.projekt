#include "NeuralNetwork.h"

#include <math.h>
#include <QtGlobal>
#include <QJsonArray>

const QString NeuralNetwork::allowedInputSymbols = "abcdefghijklmnopqrstuvwxyzáčďéěíňóřšťúůýž";

static const QMap<QChar,int> inputLayerSymbolMapping = [](){
	Q_ASSERT( NeuralNetwork::allowedInputSymbols.length() + 1 == NeuralNetwork::symbolCount );

	QMap<QChar,int> result;

	for( int i = 0; i < NeuralNetwork::allowedInputSymbols.length(); i ++ )
		result.insert( NeuralNetwork::allowedInputSymbols[i], i + 1 );

	return result;
}();

NeuralNetwork *NeuralNetwork::createFromJson(const QJsonObject &json)
{
	const int layerCount = json["layerCount"].toInt(-1);
	if( layerCount < 0 || layerCount > maxLayerCount )
		return nullptr;

	const QJsonArray layerNeuronCounts = json["layerNeuronCounts"].toArray();
	if( layerNeuronCounts.count() != layerCount || layerNeuronCounts[layerCount-1].toInt() != _wcCount )
		return nullptr;

	QVector<int> hiddenLayerNeuronCounts;
	for( int i = 0; i < layerCount - 1; i ++ ) {
		const int val = layerNeuronCounts[i].toInt(-1);
		if( val <= 0 || val > maxLayerSize )
			return nullptr;

		hiddenLayerNeuronCounts.append( val );
	}

	const int testSetPercentage = json["testSetPercentage"].toInt();
	if( testSetPercentage <= 0 || testSetPercentage >= 100 )
		return nullptr;

	QScopedPointer<NeuralNetwork> result( new NeuralNetwork( hiddenLayerNeuronCounts, testSetPercentage ) );
	result->randomSeed_ = json["randomSeed"].toInt();
	result->uniformClassLearning = json["uniformClassLearning"].toBool();

	{
		const QJsonArray jarr1 = json["neuronWeights"].toArray();
		auto &arr1 = result->neuronWeights_;

		if( jarr1.count() != arr1.count() )
			return nullptr;

		for( int x = 0; x < arr1.count(); x++ ) {
			const QJsonArray jarr2 = jarr1[x].toArray();
			auto &arr2 = arr1[x];

			if( jarr2.count() != arr2.count() )
				return nullptr;

			for( int y = 0; y < arr2.count(); y++ ) {
				const QJsonArray jarr3 = jarr2[y].toArray();
				auto &arr3 = arr2[y];

				if( jarr3.count() != arr3.count() )
					return nullptr;

				for( int z = 0; z < arr3.count(); z++ ) {
					const double val = jarr3[z].toDouble( NAN );
					if( val == NAN )
						return nullptr;

					arr3[z] = val;
				}
			}
		}
	}

	{
		const QJsonArray jarr1 = json["neuronInputs"].toArray();
		auto &arr1 = result->neuronInputs_;

		if( jarr1.count() != arr1.count() )
			return nullptr;

		for( int x = 0; x < arr1.count(); x++ ) {
			const QJsonArray jarr2 = jarr1[x].toArray();
			auto &arr2 = arr1[x];

			if( jarr2.count() != arr2.count() )
				return nullptr;

			for( int y = 0; y < arr2.count(); y++ ) {
				const double val = jarr2[y].toDouble( NAN );
				if( val == NAN )
					return nullptr;

				arr2[y] = val;
			}
		}
	}

	result->recalculateNetwork();
	return result.take();
}

NeuralNetwork::NeuralNetwork(const QVector<int> &hiddenLayerNeuronCounts, int testSetPercentage)
{
	layerCount_ = hiddenLayerNeuronCounts.length() + 1;
	testSetPercentage_ = testSetPercentage;

	layerNeuronCounts_.resize( layerCount_ );
	neuronWeights_.resize( layerCount_ );
	learningDeltaWeights_.resize( layerCount_ );
	learningDeltaValue_.resize( layerCount_ );

	neuronInputs_.resize( layerCount_ + 1 );
	maxLayerNeuronCount_ = 0;

	// Randomize configuration ID so when creating ak new network, it should always force classifications to regenerate
	configurationId_ = qrand();
	randomSeed_ = qrand();

	for( int layer = 0; layer < layerCount_; layer ++ ) {
		// Special case for output layer
		const int layerNeuronCount = ( layer == layerCount_ - 1 ) ? _wcCount : hiddenLayerNeuronCounts[ layer ];
		auto &neuronWeights = neuronWeights_[layer];
		auto &learningAccumulator = learningDeltaWeights_[layer];

		layerNeuronCounts_[layer] = layerNeuronCount;

		neuronWeights.resize( layerNeuronCount );
		learningAccumulator.resize( layerNeuronCount );
		learningDeltaValue_[layer].resize( layerNeuronCount );

		if( layerNeuronCount > maxLayerNeuronCount_ )
			maxLayerNeuronCount_ = layerNeuronCount;

		const int layerInputCount = ( layer == 0 ? inputLayerSize : layerNeuronCounts_[layer - 1] ) + 1;
		neuronInputs_[layer].resize( layerInputCount );

		for( int neuron = 0; neuron < layerNeuronCount; neuron ++ ) {
			neuronWeights[neuron].resize( layerInputCount );
			learningAccumulator[neuron].resize( layerInputCount );
		}
	}

	neuronInputs_[layerCount_].resize( layerNeuronCounts_[layerCount_ - 1] );
}

WordClass NeuralNetwork::classifyWord(const QString &word)
{
	setupInputs( word );
	recalculateNetwork();

	return (WordClass) activeOutput_;
}

void NeuralNetwork::randomizeWeights()
{
	for( int layer = 0; layer < layerCount_; layer ++ ) {
		auto &&list1 = neuronWeights_[layer];
		const float weightCount = layerWeightCount( layer ) + 1;
		const float randCoef = 2.0f * 3.0f / sqrt( weightCount );

		for( auto &&list2 : list1 ) {
			for( auto&& item : list2 )
				item = ( qrand() / float(RAND_MAX) - 0.5f ) * randCoef;
		}
	}

	configurationId_ ++;
	learnedSampleCount = 0;
}

void NeuralNetwork::randomizeFirstLayerWeights_OtherSetToIdentity()
{
	{
		const int layer = 0;
		auto &&list1 = neuronWeights_[layer];
		const float weightCount = layerWeightCount( layer ) + 1;
		const float randCoef = 2.0f * 3.0f / sqrt( weightCount );

		for( auto &&list2 : list1 ) {
			for( auto&& item : list2 )
				item = ( qrand() / float(RAND_MAX) - 0.5f ) * randCoef;
		}
	}

	for( int layer = 1; layer < layerCount_; layer ++ ) {
		auto &&list1 = neuronWeights_[layer];
		const int neuronCount = layerSize( layer );
		const int weightCount = layerWeightCount( layer ) + 1;

		for( int neuron = 0; neuron < neuronCount; neuron ++ ) {
			auto &&list2 = list1[ neuron ];

			for( int weight = 0; weight < weightCount; weight ++ )
				list2[weight] = ( weight == neuron ) ? 1.0f : (0.1f / weightCount);
		}
	}

	configurationId_ ++;
	learnedSampleCount = 0;
}

void NeuralNetwork::zeroWeights()
{
	for( auto &&list1 : neuronWeights_ ) {
		for( auto &&list2 : list1 ) {
			list2.fill( 0 );
		}
	}

	configurationId_ ++;
	learnedSampleCount = 0;
}

QJsonObject NeuralNetwork::toJson()
{
	QJsonObject result;
	{
		result["layerCount"] = layerCount_;
		result["randomSeed"] = (int) randomSeed_;
		result["testSetPercentage"] = testSetPercentage_;
		result["uniformClassLearning"] = uniformClassLearning;
	}

	{
		QJsonArray layerNeuronCounts;
		for( auto &&cnt : layerNeuronCounts_ )
			layerNeuronCounts.append( cnt );
		result["layerNeuronCounts"] = layerNeuronCounts;
	}

	{
		QJsonArray neuronWeights;
		for( auto &&list1 : neuronWeights_ ) {
			QJsonArray arr1;
			for( auto &&list2 : list1 ) {
				QJsonArray arr2;
				for( auto &&weight : list2 )
					arr2.append( weight );

				arr1.append( arr2 );
			}

			neuronWeights.append( arr1 );
		}
		result["neuronWeights"] = neuronWeights;
	}

	{
		QJsonArray neuronInputs;
		for( auto &&list1 : neuronInputs_ ) {
			QJsonArray arr1;
			for( auto &&input : list1 )
				arr1.append( input );

			neuronInputs.append( arr1 );
		}
		result["neuronInputs"] = neuronInputs;
	}

	return result;
}

void NeuralNetwork::prepareLearning()
{
	for( auto &&list1 : learningDeltaWeights_ ) {
		for( auto &&list2 : list1 )
			list2.fill( 0 );
	}
}

bool NeuralNetwork::learnSample(const ClassificationSample &sample)
{
	WordClass calculatedClass = classifyWord( sample.word() );

	// First recalculate last layer
	{
		const int layer = layerCount_ - 1;
		const int weightCount = layerWeightCount( layer );
		const int currentLayerSize = layerSize( layer );

		auto &deltaWeights = learningDeltaWeights_[layer];
		auto &deltaVals = learningDeltaValue_[layer];
		auto &neuronVals = neuronInputs_[layer + 1];
		auto &neuronInputs = neuronInputs_[layer];

		for( int neuron = 0; neuron < currentLayerSize; neuron ++ ) {
			auto &neuronDeltaWeights = deltaWeights[neuron];

			const float expectedNeuronValue = ( sample.wordClass() == (WordClass) neuron ) ? 1.0f : 0.0f;
			const float actualNeuronValue = neuronVals[neuron];

			// Calculate delta value
			const float deltaValue = ( expectedNeuronValue - actualNeuronValue ) * activationFunctionDerivation_( actualNeuronValue );
			deltaVals[ neuron ] = deltaValue;

			const float base = deltaValue * learningCoef;
			for( int weight = 0; weight < weightCount; weight ++ )
				neuronDeltaWeights[weight] += base * neuronInputs[weight];

			// And last, 1-const-value weight
			neuronDeltaWeights[weightCount] += base;
		}
	}

	// Then every other
	for( int layer = layerCount_ - 2; layer >= 0; layer -- ){
		const int weightCount = layerWeightCount( layer );
		const int currentLayerSize = layerSize( layer );
		const int nextLayerSize = layerSize( layer + 1 );

		auto &deltaWeights = learningDeltaWeights_[layer];
		auto &deltaVals = learningDeltaValue_[layer];
		auto &nextLayerDeltaVals = learningDeltaValue_[layer+1];
		auto &nextLayerWeights = neuronWeights_[layer + 1];
		auto &neuronVals = neuronInputs_[layer + 1];
		auto &neuronInputs = neuronInputs_[layer];

		for( int neuron = 0; neuron < currentLayerSize; neuron ++ ) {
			auto &neuronDeltaWeiths = deltaWeights[neuron];

			// Calculate delta value
			float deltaValue = 0;

			for( int nextLayerNeuron = 0; nextLayerNeuron < nextLayerSize; nextLayerNeuron++ )
				deltaValue += nextLayerDeltaVals[ nextLayerNeuron ] * nextLayerWeights[ nextLayerNeuron ][ neuron ];

			deltaValue *= activationFunctionDerivation_( neuronVals[neuron] );
			deltaVals[ neuron ] = deltaValue;

			const float base = deltaValue * learningCoef;
			for( int weight = 0; weight < weightCount; weight ++ )
				neuronDeltaWeiths[weight] += base * neuronInputs[weight];

			// And last, 1-const-value weight
			neuronDeltaWeiths[weightCount] += base;
		}
	}

	return calculatedClass == sample.wordClass();
}

void NeuralNetwork::applyLearning()
{
	for( int layer = 0; layer < layerCount_; layer ++ ) {
		const int neuronCount = layerSize( layer );
		const int weightCount = layerWeightCount( layer );

		auto &layerWeights = neuronWeights_[layer];
		auto &layerDeltas = learningDeltaWeights_[layer];

		for( int neuron = 0; neuron < neuronCount; neuron ++ ) {
			auto &neuronWeights = layerWeights[neuron];
			auto &neuronDeltas = layerDeltas[neuron];

			for( int weight = 0; weight < weightCount; weight ++ )
				neuronWeights[weight] += neuronDeltas[weight];

			// 1-const value weight
			neuronWeights[weightCount] += neuronDeltas[weightCount];
		}
	}

	configurationId_ ++;
}

void NeuralNetwork::setupInputs(const QString &iword)
{
	Q_ASSERT( iword.length() <= maxInputStringLength );

	QString word = iword.toLower();

	// Setup inputs
	{
		for( auto &input : neuronInputs_[0] )
			input = 0;

		for( int i = 0; i < word.length(); i ++ )
			neuronInputs_[0][i*symbolCount + inputLayerSymbolMapping.value( word[i], 0 )] = 1.0f;

		for( int i = word.length(); i < maxInputStringLength; i ++ )
			neuronInputs_[0][i*symbolCount] = 1.0f;
	}
}

void NeuralNetwork::recalculateNetwork()
{
	for( int layer = 0; layer < layerCount_; layer ++ ) {
		auto &inputs = neuronInputs_[layer];
		auto &outputs = neuronInputs_[layer+1];

		int neuronCount = layerNeuronCounts_[layer];
		int weightCount = layerWeightCount( layer );

		for( int neuron = 0; neuron < neuronCount; neuron ++ ) {
			auto &weights = neuronWeights_[layer][neuron];
			float val = 0;

			for( int weightId = 0; weightId < weightCount; weightId ++ )
				val += weights[weightId] * inputs[weightId];

			// Add constant-value weight
			val += weights[weightCount];

			outputs[neuron] = activationFunction_( val );
		}
	}

	// Get output with maximum value
	{
		auto &outputValues = neuronInputs_[layerCount_];

		float max = outputValues[0];
		activeOutput_ = 0;

		for( int i = 1; i < outputValues.length(); i ++ ) {
			if( outputValues[i] > max ) {
				max = outputValues[i];
				activeOutput_ = i;
			}
		}
	}
}
