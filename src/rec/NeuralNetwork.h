#ifndef NEURALNETWORK_H
#define NEURALNETWORK_H

#include <math.h>
#include <stdlib.h>
#include <functional>
#include <QVector>
#include <QJsonObject>

#include "rec/WordClass.h"
#include "rec/ClassificationSample.h"

class NeuralNetwork
{

public:
	static const int maxLayerCount = 16;
	static const int maxLayerSize = 256;

	static const int symbolCount = 42;
	static const int maxInputStringLength = 10;
	static const int inputLayerSize = symbolCount * maxInputStringLength;
	static const QString allowedInputSymbols;

public:
	/// Returns false on error
	static NeuralNetwork *createFromJson( const QJsonObject &json );

	NeuralNetwork( const QVector<int> &hiddenLayerNeuronCounts, int testSetPercentage );

public:
	int layerCount() const {
		return layerCount_;
	}

	/// Returns number of neurons on given layer (0 = first hidden layer)
	int layerSize( int layer ) const {
		return layerNeuronCounts_[layer];
	}

	/// Returns number of weights for each neuron in the given layer (0 = first hidden layer)
	int layerWeightCount( int layer ) const {
		return layer == 0 ? inputLayerSize : layerSize( layer - 1 );
	}

	/// Maximum of neuron counts in each layer
	int maxLayerNeuronCount() const {
		return maxLayerNeuronCount_;
	}

	float neuronWeight( int layer, int neuron, int weight ) const {
		return neuronWeights_[layer][neuron][weight];
	}

	float inputValue( int input ) const {
		return neuronInputs_[0][input];
	}

	float neuronValue( int layer, int neuron ) const {
		return neuronInputs_[layer + 1][neuron];
	}

	size_t configurationId() const {
		return configurationId_;
	}

	int activeOutput() const {
		return activeOutput_;
	}

	bool isSampleInTestSet( const ClassificationSample &sample ) {
		return ( ( sample.wordHash() ^ randomSeed_ ) % 100 ) <= (uint32_t) testSetPercentage_;
	}

public:
	WordClass classifyWord( const QString &word );

	void randomizeWeights();
	void randomizeFirstLayerWeights_OtherSetToIdentity();
	void zeroWeights();

	QJsonObject toJson();

public:
	void prepareLearning();

	/// Learns one sample; the changes are applied after calling applyLearning(); returns if the sample was classified correctly or not
	bool learnSample( const ClassificationSample &sample );

	/// Applies all weight changes accumulated by previous learSample calls
	void applyLearning();

private:
	/// Sets network inputs to correspond with the given word
	void setupInputs( const QString &word );
	void recalculateNetwork();

public:
	float learningCoef = 0.5;
	/// Learn samples are distributed uniformly by word class
	bool uniformClassLearning = false;
	size_t learnedSampleCount = 0;

private:
	int layerCount_;

	/// Maximum of neuron counts in each layer
	int maxLayerNeuronCount_;

	/// Number of neurons in each layer
	QVector<int> layerNeuronCounts_;

	/// [ layer, neuron, weight ]; weight [size-1] is const weight
	QVector<QVector<QVector<float>>> neuronWeights_;

	/// Inputs for neurons in given layer (outputs from previous layer, except neuronInputs_[0] goes real from inputs); neuronInputs_[layerCount_+1] = output
	QVector<QVector<float>> neuronInputs_;

	/// Each time weights or other properties are changed, this variable is increased. This is to detect when inputs need reclassification
	size_t configurationId_;

	/// Output with the highest value
	int activeOutput_ = 0;

	/// Random seed used for separating samples into test & training groups
	uint32_t randomSeed_;

	/// Percentage of how manym samples belongs to the test set (the rest is training set)
	int testSetPercentage_ = 80;

	std::function<float(float)> activationFunction_ = [](float x){ return 1.0f / ( 1.0f + exp( -x ) ); };
	std::function<float(float)> activationFunctionDerivation_ = [](float y){ return y * ( 1.0f - y ); };

private:
	/// Accumulator for learning changes
	QVector<QVector<QVector<float>>> learningDeltaWeights_;

	/// Delta values for learning
	QVector<QVector<float>> learningDeltaValue_;

};

#endif // NEURALNETWORK_H
