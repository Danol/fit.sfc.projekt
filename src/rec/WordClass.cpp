#include "WordClass.h"

const QMap<WordClass,QString> wordClassNames {
	{wcPodstatneJm, "podstatné jméno"},
	{wcPridavneJm, "přídavné jméno"},
	{wcZajmeno, "zájmeno"},
	{wcCislovka, "číslovka"},
	{wcSloveso, "sloveso"},
	{wcPrislovce, "příslovce" },
	{wcPredlozka, "předložka" },
	{wcSpojka, "spojka" },
	{wcCastice, "částice" },
	{wcCitoslovce, "citoslovce" }
};

const QMap<WordClass,QString> capitalizedWordClassNames = [](){
	QMap<WordClass,QString> result;

	for( WordClass wc : wordClassNames.keys() ) {
		QString str = wordClassNames[wc];
		result.insert( wc, str.left(1).toUpper() + str.mid(1) );
	}

	return result;
}();
