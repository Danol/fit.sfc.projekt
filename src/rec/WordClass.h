#ifndef WORDCLASS_H
#define WORDCLASS_H

#include <QMap>
#include <QString>

enum WordClass {
	wcPodstatneJm,
	wcPridavneJm,
	wcZajmeno,
	wcCislovka,
	wcSloveso,
	wcPrislovce,
	wcPredlozka,
	wcSpojka,
	wcCastice,
	wcCitoslovce,
	_wcCount
};

extern const QMap<WordClass,QString> wordClassNames;
extern const QMap<WordClass,QString> capitalizedWordClassNames;

#endif // WORDCLASS_H
