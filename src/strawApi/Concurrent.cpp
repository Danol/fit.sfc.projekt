#include "Concurrent.h"

#include <QtConcurrent/QtConcurrent>
#include <QMutexLocker>
#include <QApplication>

Concurrent *concurrent;

Concurrent::Concurrent() {
	connect(&mainThreadJobTimer_,SIGNAL(timeout()),this,SLOT(onMainThreadJobTimerTimeout()));

	mainThreadJobTimer_.setInterval( 10 );
	mainThreadJobTimer_.start();
}

void Concurrent::runThreaded(const ConcurrentFunction &func, bool wait)
{
	{
		QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
		nonSilentRunningJobsCount_ ++;
	}

	auto future = QtConcurrent::run([=]{
		func();

		QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
		nonSilentRunningJobsCount_ --;
		nonSilentRunningJobsCountCondition_.wakeAll();
	});
	if( wait )
		future.waitForFinished();
}

void Concurrent::runThreadedSilent(const ConcurrentFunction &func, bool wait)
{
	auto future = QtConcurrent::run( func );
	if( wait )
		future.waitForFinished();
}

void Concurrent::runOnMainThread(const ConcurrentFunction &func, bool wait)
{
	if( QThread::currentThread() == qApp->thread() ) {
		func();
		return;
	}

	{
		QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
		nonSilentRunningJobsCount_ ++;
	}

	if( !wait ) {
		ConcurrentFunction func_ = func;

		QMutexLocker ml(&mainThreadJobQueueMutex_);
		mainThreadJobQueue_.enqueue( [=]{
			func_();

			QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
			nonSilentRunningJobsCount_ --;
			nonSilentRunningJobsCountCondition_.wakeAll();
		} );
		return;
	}

	bool done = false;
	QMutex mut;
	QWaitCondition cond;

	{
		QMutexLocker ml(&mainThreadJobQueueMutex_);
		mainThreadJobQueue_.enqueue([&]{
			func();

			QMutexLocker ml2(&mut);
			done = true;
			cond.wakeAll();

			QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
			nonSilentRunningJobsCount_ --;
			nonSilentRunningJobsCountCondition_.wakeAll();
		});
	}

	{
		QMutexLocker ml(&mut);
		if( !done )
			cond.wait( &mut );
	}
}

QFuture<void> Concurrent::runThreadedDedicated(const ConcurrentFunction &func, bool wait)
{
	{
		QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
		nonSilentRunningJobsCount_ ++;
	}

	QThreadPool::globalInstance()->reserveThread();
	auto future = QtConcurrent::run([=]{
		func();
		QThreadPool::globalInstance()->releaseThread();

		QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
		nonSilentRunningJobsCount_ --;
		nonSilentRunningJobsCountCondition_.wakeAll();
	});
	if( wait )
		future.waitForFinished();

	return future;
}

void Concurrent::wait()
{
	QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
	while( nonSilentRunningJobsCount_ > 0 )
		nonSilentRunningJobsCountCondition_.wait( &nonSilentRunningJobsCountMutex_ );
}

void Concurrent::waitWithProcessingEvents()
{
	while( true ) {
		{
			QMutexLocker ml(&nonSilentRunningJobsCountMutex_);
			if( nonSilentRunningJobsCount_ == 0 )
				return;
		}

		QApplication::processEvents();
		QThread::msleep( 10 );
	}
}

void Concurrent::onMainThreadJobTimerTimeout()
{
	ConcurrentFunction f;

	while( true ) {
		{
			QMutexLocker ml(&mainThreadJobQueueMutex_);
			if( mainThreadJobQueue_.isEmpty() )
				return;

			f	= mainThreadJobQueue_.dequeue();
		}

		f();
	}
}
