#ifndef CONCURRENT_H
#define CONCURRENT_H

#include <functional>
#include <QMutex>
#include <QObject>
#include <QWaitCondition>
#include <QQueue>
#include <QTimer>
#include <QFuture>

using ConcurrentFunction = std::function<void()>;

/// Helper class for multi threaded tasks
class Concurrent : public QObject
{
	Q_OBJECT

public:
	Concurrent();

public:
	/// Executes the function on a thread
	void runThreaded( const ConcurrentFunction &func, bool wait = false );

	/// Executes the function on a thread; the program does not wait for this function to finish to start up
	void runThreadedSilent( const ConcurrentFunction &func, bool wait = false );

	/// Executes the function on the main thread
	void runOnMainThread( const ConcurrentFunction &func, bool wait = false );

	/// Executes the function on a thread; this thread will not be counted into thread pool's max thread limit
	QFuture<void> runThreadedDedicated( const ConcurrentFunction &func, bool wait = false );

	/// Waits for all non-silent threaded jobs to finish
	void wait();

	/// Waits for all non-silent threaded jobs to finish, processing events in the meantime
	void waitWithProcessingEvents();

private slots:
	void onMainThreadJobTimerTimeout();

private:
	int nonSilentRunningJobsCount_ = 0;
	QMutex nonSilentRunningJobsCountMutex_;
	QWaitCondition nonSilentRunningJobsCountCondition_;

private:
	QQueue<ConcurrentFunction> mainThreadJobQueue_;
	QMutex mainThreadJobQueueMutex_;
	QTimer mainThreadJobTimer_;

};

extern Concurrent *concurrent;

#endif // CONCURRENT_H
