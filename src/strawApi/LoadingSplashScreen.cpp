#include "LoadingSplashScreen.h"
#include "ui_LoadingSplashScreen.h"

#include <QCloseEvent>
#include <QDesktopWidget>

#include <strawApi/Concurrent.h>

LoadingSplashScreen::LoadingSplashScreen(QWidget *parent) :
	QDialog(parent),
	ui(new Ui::LoadingSplashScreen),
	animation_( ":/strawApi/loader.gif" )
{
	ui->setupUi(this);
	updateFlags();
	setStartupMode( false );

	ui->lblProductName->setText( QString( "SFC Projekt | xcejch00" ) );

	animation_.start();
	connect( &animation_, SIGNAL(frameChanged(int)), this, SLOT(onAnimation(int)) );
}

LoadingSplashScreen::~LoadingSplashScreen()
{
	delete ui;
}

void LoadingSplashScreen::changeEvent(QEvent *e)
{
	QDialog::changeEvent(e);
	switch (e->type()) {
	case QEvent::LanguageChange:
		ui->retranslateUi(this);
		break;
	default:
		break;
	}
}
void LoadingSplashScreen::closeEvent(QCloseEvent *e) {
	if( !canClose_ )
		e->ignore();

	QDialog::closeEvent(e);
}

void LoadingSplashScreen::reject() {
	if( canClose_ )
		QDialog::reject();
}

void LoadingSplashScreen::updateLabelsAndShow()
{
	ui->lblText->setText( loadingTexts_.last() );
	ui->lblLoadingTexts->setText( loadingTexts_.values().join( '\n' ) );

	QDialog::show();
}

void LoadingSplashScreen::onAnimation(int) {
	if( isVisible() )
		ui->lblAnimation->setPixmap( animation_.currentPixmap() );
}

int LoadingSplashScreen::show( const QString &text ) {
	loadingTexts_.insert( idCounter_, text + "..." );

	canClose_ = false;
	updateLabelsAndShow();

	return idCounter_ ++;
}

void LoadingSplashScreen::close(int closeId) {
	concurrent->runOnMainThread([this, closeId]{
		loadingTexts_.remove( closeId );

		if( loadingTexts_.isEmpty() ) {
			canClose_ = true;
			QDialog::close();

		} else
			updateLabelsAndShow();
	});
}

void LoadingSplashScreen::setStartupMode(bool set)
{
	ui->stackedWidget->setCurrentWidget( set ? ui->pageStartup : ui->pageDefault );
}

void LoadingSplashScreen::updateFlags()
{
	setFixedSize( size() );
	setWindowFlags( Qt::CustomizeWindowHint | Qt::WindowTitleHint | Qt::Window | Qt::FramelessWindowHint );
	setGeometry( QStyle::alignedRect( Qt::LeftToRight, Qt::AlignCenter, size(), qApp->desktop()->availableGeometry() ) );
}
