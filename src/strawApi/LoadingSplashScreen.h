#ifndef LOADINGSPLASHSCREEN_H
#define LOADINGSPLASHSCREEN_H

#include <QDialog>
#include <QMovie>
#include <QMap>
#include <QTimer>

namespace Ui {
	class LoadingSplashScreen;
}

class LoadingSplashScreen : public QDialog
{
	Q_OBJECT

public:
	explicit LoadingSplashScreen(QWidget *parent = 0);
	~LoadingSplashScreen();

public:
	/**
	 * @brief This function is NOT thread-safe
	 * @param text
	 * @return Text id used in closing
	 */
	int show( const QString &text );
	/**
	 * @brief This function is thread-safe
	 * @param closeId
	 */
	void close( int closeId );

	void setStartupMode( bool set );
	void updateFlags();

protected:
	void changeEvent(QEvent *e);
	void closeEvent(QCloseEvent *e) override;
	void reject() override;

private:
	void updateLabelsAndShow();

private slots:
	void onAnimation( int frame );

private:
	QMap<int,QString> loadingTexts_;
	Ui::LoadingSplashScreen *ui;
	bool canClose_ = true;
	int idCounter_ = 0;
	QMovie animation_;

};

#endif // LOADINGSPLASHSCREEN_H
